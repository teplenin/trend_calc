var calcApp = angular.module('calcApp', []);

calcApp.service('createElementsFromProps', function() {
    return function(props) {
        this.elements = [];

        this.createSelectFromProp = function(prop) {
            var element, label, select;

            label = '<div class="cform__line-label">'+prop.name+'</div>';
            select = '<select id="'+prop.id+'" class="cform__line-select" name="'+prop.sendName+'" ng-model="'+prop.sendName+'Selected" ng-options="option.name for option in '+prop.sendName+' track by option.id"></select>';

            element = '<div class="cform__line">'+label+select+'</div>';

            return element;
        }

        this.createInputFromProp = function(prop) {
            var element, label, input, directive;

            switch(prop.id) {
                case 'price':
                case 'first_pay':
                    directive = 'format="number"';

                    break;
            }

            label = '<div class="cform__line-label">'+prop.name+'</div>';
            input = '<input id="'+prop.id+'" name="'+prop.sendName+'" type="text" class="cform__line-input" ng-model="'+prop.sendName+'"'+(directive ? ' '+directive : '')+'>';

            element = '<div class="cform__line">'+label+input+'</div>';

            return element;
        }

        angular.forEach(props, function(prop) {
            var element = {
                html: '',
                attrs: prop
            };

            switch(prop.type) {
                case 'select':
                    element.html = createSelectFromProp(prop);

                    break;
                case 'input':
                    element.html = createInputFromProp(prop);

                    break;
            }

            this.elements.push(element);
        });

        return elements;
    }
});

calcApp.directive('calc', ['$http', '$compile', '$timeout', 'createElementsFromProps', function($http, $compile, $timeout, createElementsFromProps) {
    return {
        templateUrl: 'calc.tpl.html',
        scope: {
            'selectProgram': '&'
        },
        link: function ($scope, element, attrs, controller) {
            $scope.programs = [];
            $scope.props = [];
            $scope.programSelected = {};
            $scope.form = angular.element(element[0].getElementsByClassName('calc__form'));
            $scope.propsBlock = angular.element($scope.form[0].getElementsByClassName('calc__form-props'));

            $scope.selectProgram = function($index) {
                $scope.props = $scope.programSelected.props;
            }

            $scope.submit = function() {
                var url;
                url = 'https://api.trend-spb.ru/v2/terminal/installments/';
                url += '?programm_id='+$scope.programSelected.id;

                angular.forEach($scope.props, function(prop) {
                    switch(prop.type) {
                        case 'select':
                            url += '&'+prop.sendName+'='+encodeURIComponent($scope[prop.sendName+'Selected'].value);

                            break;
                        case 'input':
                            url += '&'+prop.sendName+'='+parseFloat($scope[prop.sendName]).toFixed();

                            break;
                    }
                });

                $scope.result = {
                    program: null,
                    pay: null,
                    summ: null,
                    errors: []
                }

                $http.get(url)
                    .success(function(response) {
                        if(response.data.errors.length) {
                            void 0;
                            angular.forEach(response.data.errors, function(error) {

                                $scope.result.errors.push({
                                    msg: error.description
                                });
                            });
                        } else {
                            $scope.result.program = response.data.title;
                            $scope.result.pay = response.data.values[0].title;
                            $scope.result.summ = response.data.values[0].value;
                        }
                    })
                    .error(function(response) {
                        if(response.errors) {
                            angular.forEach(response.errors, function(error) {

                                $scope.result.errors.push({
                                    msg: error.msg
                                });
                            });
                        }
                    });
            }

            $scope.$watchCollection('props', function(props) {
                angular.forEach(props, function(prop) {
                    switch(prop.type) {
                        case 'select':
                            var values = [];

                            angular.forEach(prop.values, function(value, key) {
                                var option = {
                                        'id': key,
                                        'value': value.value,
                                        'name': value.name
                                    };

                                if(value.min_price) {
                                    option.price = value.min_price;
                                }

                                values.push(option);
                            });

                            $scope[prop.sendName+'Selected'] = values[0];
                            $scope[prop.sendName] = values;

                            break;
                    }
                });

                $scope.propsBlock.empty();

                var propsElements = createElementsFromProps(props);
                angular.forEach(propsElements, function(element) {
                    $scope.propsBlock.append($compile(element.html)($scope));
                });

                $scope.$watchCollection('date_endSelected', function(build) {
                    if(!build) return;

                    $scope.price = build.price;
                });
            });


            $http.get('https://api.trend-spb.ru/v2/terminal/installments/286/list/').
                success(function(data, status, headers, config) {
                    $scope.programs = data.data;
                    $scope.props = $scope.programs[0].props;
                    $scope.programSelected = $scope.programs[0];
                });
        }
    }
}]);

calcApp.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });

            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);